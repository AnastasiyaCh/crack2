#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{    

    // Hash the guess using MD5
    char *guessHash = md5(guess, strlen(guess));
    strcat(guessHash, "\n");
    int flag = 1;
    
    // Compare the two hashes
    if (strcmp(hash, guessHash) != 0) {
        flag = 0;
    }

    // Free any malloc'd memory
    free(guessHash);
    
    return flag;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{   
    struct stat info;
    *size = 0;
    int i = 1;

    // find out how big the file is
    if (stat(filename, &info) == -1) {
        puts("Can't stat the file.");
        exit(1);
    }
    
    int filesize = info.st_size;

    // allocate memory for file
    // add one for null terminator
    char *contents = malloc(filesize + 1);
    
    // open file to read information
    FILE *dictionary = fopen(filename, "rb");
    if ( !dictionary ) {
        puts("File can't be open for reading.");
        exit(1);
    }
    
    // read contents of the file into contents
    fread(contents, 1, filesize, dictionary);
    
    //close file
    fclose(dictionary);
    
    // add null terminator character at the end of the string
    contents[filesize] = '\0';
    
    // count new lines to find an array size
    for (int j = 0; j < filesize; j++) {
        if (contents[j] == '\n') { (*size)++; }
    }

    // allocate memory for array of passwords
    char **pswd = malloc(sizeof(char *) * (*size));
    
    // fill in array with pointers
    pswd[0] = strtok(contents, "\n");
    while( (pswd[i] = strtok(NULL, "\n")) != NULL ) {
        i++;
    }

    return pswd;
}


int main(int argc, char *argv[])
{

    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");

    // For each hash, try every entry in the dictionary.
    char hash[40];
    while ( fgets(hash, 40, hashFile) != NULL) {
 
        int i = 0;
        while ((i < dlen) && (tryguess(hash, dict[i]) != 1) ) {
            i++;
        }
        
        if (i < dlen) {
            printf("password: %s, hash: %s", dict[i], hash);
        }
    }
    free(dict[0]);
    free(dict);
}
